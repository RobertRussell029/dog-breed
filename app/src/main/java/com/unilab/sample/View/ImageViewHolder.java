package com.unilab.sample.View;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.unilab.sample.R;

public class ImageViewHolder extends RecyclerView.ViewHolder {
    ImageView image;
    TextView title;

    public ImageViewHolder(@NonNull View itemView) {
        super(itemView);

        image = (ImageView) itemView.findViewById(R.id.iv_dog_pic);
        title = (TextView) itemView.findViewById(R.id.tv_title);
    }
}
