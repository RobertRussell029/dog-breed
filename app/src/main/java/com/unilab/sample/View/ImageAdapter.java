package com.unilab.sample.View;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.unilab.sample.R;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageViewHolder>{
    Context context;
    List<String> randomPic;

    Dialog dialogViewImage;

    public ImageAdapter(Context context, List<String> randomPicModels) {
        this.context = context;
        this.randomPic = randomPicModels;

        Log.i("model-count", randomPic.size() + "");
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_adapter, viewGroup, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int i) {
        final String picture = randomPic.get(i);
        //String picture = "https://images.dog.ceo/breeds/hound-afghan/n02088094_1370";
        Log.i("URL--", picture);
        //imageViewHolder.title.setText(picture);

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round);

        Glide.with(context).load(picture).apply(options).into(imageViewHolder.image);

        imageViewHolder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogViewImage(picture);
            }
        });
    }

    @Override
    public int getItemCount() {
        Log.i("model-count-get-count", randomPic.size() + "");
        return randomPic.size();
    }

    public void dialogViewImage(String url) {
        dialogViewImage = new Dialog(context);
        dialogViewImage.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialogViewImage.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogViewImage.setCancelable(true);
        dialogViewImage.setContentView(R.layout.dialog_view_image);
        dialogViewImage.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ImageView pic = (ImageView) dialogViewImage.findViewById(R.id.tv_image_zoom);

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round);

        Glide.with(context).load(url).apply(options).into(pic);

        dialogViewImage.show();
    }
}
