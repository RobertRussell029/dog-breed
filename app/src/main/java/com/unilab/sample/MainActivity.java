package com.unilab.sample;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.unilab.sample.Retrofit.RetrofitClient;
import com.unilab.sample.View.ImageAdapter;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    Context context;
    public static Context statCon;

    public static Spinner spnBreed;
    public static Spinner spnSubBreed;

    Button btnFilter;
    static RecyclerView rvContent;

    String breed = "", sub_breed = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        statCon = this;

        if (isNetworkAvailable()) {
            RetrofitClient.getRandomPic(this);
        } else {
            Toast.makeText(context, "No internet connection!", Toast.LENGTH_SHORT).show();
        }

        spnBreed = (Spinner) findViewById(R.id.spn_breed);
        spnSubBreed = (Spinner) findViewById(R.id.spn_sub_breed);
        btnFilter = (Button) findViewById(R.id.btn_filter);
        rvContent = (RecyclerView) findViewById(R.id.rv_content);

        spnBreed.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedBreed = spnBreed.getSelectedItem().toString();
                RetrofitClient.getSubBreed(context, selectedBreed);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                breed = "";
                sub_breed = "";

                if (spnBreed != null && spnBreed.getSelectedItem() != null) {
                    breed = spnBreed.getSelectedItem().toString();
                }

                if (spnSubBreed != null && spnSubBreed.getSelectedItem() != null) {
                    sub_breed = spnSubBreed.getSelectedItem().toString();
                }

                if (isNetworkAvailable()) {
                    if (!breed.equals("") && !sub_breed.equals("")) {
                        //Toast.makeText(context, "Breed: " + breed + " Sub Breed: " + sub_breed, Toast.LENGTH_SHORT).show();
                        RetrofitClient.getSearch(context, breed, sub_breed);
                    } else if (!breed.equals("")){
                        //Toast.makeText(context, "Breed: " + breed, Toast.LENGTH_SHORT).show();
                        RetrofitClient.getSearch(context, breed, sub_breed);
                    } else {
                        RetrofitClient.getRandomPic(context);
                    }
                } else {
                    Toast.makeText(context, "No internet connection!", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public static void displayData(List<String> picModels) {
        Log.i("DISPLAY", "DATA " + picModels.get(0));
        ImageAdapter adapter = new ImageAdapter(statCon, picModels);
        rvContent.setLayoutManager(new LinearLayoutManager(statCon));
        rvContent.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
