package com.unilab.sample.Retrofit;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.unilab.sample.MainActivity;
import com.unilab.sample.Model.BreedModel;
import com.unilab.sample.Model.RandomPicModel;
import com.unilab.sample.Model.SearchModel;
import com.unilab.sample.Model.SubBreedModel;
import com.unilab.sample.Utils.Glovar;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static final String TAG = "RETROCLIENT";
    private static Retrofit ourInstance;
    private static Retrofit subBreedInstance;
    private static Retrofit randomInstance;
    private static Retrofit searchInstance;

    public static Context context;
    static ProgressDialog progressDoalog;

    public static Retrofit getBreed(Context con) {
        context = con;

        if (ourInstance == null) {
            ourInstance = new Retrofit.Builder()
                    .baseUrl("https://dog.ceo/api/breeds/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }

        // Set up progress before call
        progressDoalog = new ProgressDialog(context);
        progressDoalog.setMessage("Loading data, please wait...");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();

        ApiInterface apiInterface = ourInstance.create(ApiInterface.class);

        try {
            apiInterface.getBreed()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<BreedModel>() {
                        @Override
                        public void accept(BreedModel breedModel) throws Exception {
                            List<String> breedModels = breedModel.getMessage();
                            setBreedSpinner(breedModels);

                            progressDoalog.dismiss();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            Log.i("CATCH", "MESSAGE " + e.getMessage());
            progressDoalog.dismiss();
        }

        return ourInstance;
    }

    public static Retrofit getSubBreed(final Context con, String breed) {
        context = con;
        String selectedBreed = "https://dog.ceo/api/breed/" + breed + "/";
        Log.i("BREED--", selectedBreed);

        subBreedInstance = new Retrofit.Builder()
                .baseUrl(selectedBreed)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        // Set up progress before call
        progressDoalog = new ProgressDialog(context);
        progressDoalog.setMessage("Loading data, please wait...");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();

        ApiInterface subBreedInterface = subBreedInstance.create(ApiInterface.class);

        try {
            subBreedInterface.getSubBreed()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<SubBreedModel>() {
                        @Override
                        public void accept(SubBreedModel subBreedModel) throws Exception {
                            List<String> subBreedModels = subBreedModel.getMessage();
                            setSubBreedSpinner(subBreedModels);
                            progressDoalog.dismiss();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            Log.i("CATCH", "MESSAGE " + e.getMessage());
            progressDoalog.dismiss();
        }

        return subBreedInstance;
    }

    public static Retrofit getRandomPic(final Context con) {
        context = con;
        String BASED_URL = "https://dog.ceo/api/breeds/image/";

        randomInstance = new Retrofit.Builder()
                .baseUrl(BASED_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        // Set up progress before call
        progressDoalog = new ProgressDialog(context);
        progressDoalog.setMessage("Loading images, please wait...");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();

        ApiInterface randomInterface = randomInstance.create(ApiInterface.class);

        try {
            randomInterface.getRandomImage()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<RandomPicModel>() {
                        @Override
                        public void accept(RandomPicModel randomPicModel) throws Exception {
                            List<String> randomPicture = randomPicModel.getMessage();
                            Log.i("BREED-RANDOM", randomPicture.size() + "");
                            MainActivity.displayData(randomPicture);

                            progressDoalog.dismiss();

                            getBreed(con);
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            Log.i("CATCH", "MESSAGE " + e.getMessage());
            progressDoalog.dismiss();
        }

        return randomInstance;
    }

    public static Retrofit getSearch(final Context con, String breed, String subBreed) {
        context = con;

        //breed - https://dog.ceo/api/breed/hound/images
        //sub-breed - https://dog.ceo/api/breed/hound/basset/images
        String BASED_URL = "";
        Log.i("SELECTED--", breed + subBreed);

        if (!breed.equals("") && !subBreed.equals("")) {
            BASED_URL = "https://dog.ceo/api/breed/" + breed + "/" + subBreed + "/";
        } else {
            BASED_URL = "https://dog.ceo/api/breed/" + breed + "/";
        }

        searchInstance = new Retrofit.Builder()
                .baseUrl(BASED_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        // Set up progress before call
        progressDoalog = new ProgressDialog(context);
        progressDoalog.setMessage("Loading images, please wait...");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();

        ApiInterface searchInterface = searchInstance.create(ApiInterface.class);

        try {
            searchInterface.getSearchImages()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<SearchModel>() {
                        @Override
                        public void accept(SearchModel searchModel) throws Exception {
                            List<String> searchPictures = searchModel.getMessage();
                            Log.i("BREED-RANDOM", searchPictures.size() + "");

                            Log.i("BREED--RANDOM", searchModel.getStatus() + "");

                            if (!searchModel.getStatus().equals("error")) {
                                MainActivity.displayData(searchPictures);
                            } else {
                                Toast.makeText(con, "Images not found!", Toast.LENGTH_SHORT).show();
                            }

                            progressDoalog.dismiss();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            Log.i("CATCH", "MESSAGE " + e.getMessage());
            progressDoalog.dismiss();
        }

        return searchInstance;
    }

    public RetrofitClient() {
    }

    private static void setBreedSpinner(List<String> arraySpinner) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        MainActivity.spnBreed.setAdapter(adapter);
    }

    private static void setSubBreedSpinner(List<String> arraySpinner) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        MainActivity.spnSubBreed.setAdapter(adapter);
    }
}
