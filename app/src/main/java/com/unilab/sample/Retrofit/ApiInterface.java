package com.unilab.sample.Retrofit;

import com.unilab.sample.Model.BreedModel;
import com.unilab.sample.Model.RandomPicModel;
import com.unilab.sample.Model.SearchModel;
import com.unilab.sample.Model.SubBreedModel;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("list")
    Observable<BreedModel> getBreed();

    @GET("list")
    Observable<SubBreedModel> getSubBreed();

    @GET("random/20")
    Observable<RandomPicModel> getRandomImage();

    @GET("images")
    Observable<SearchModel> getSearchImages();
}
